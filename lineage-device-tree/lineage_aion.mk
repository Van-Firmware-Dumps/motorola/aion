#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Inherit from those products. Most specific first.
$(call inherit-product, $(SRC_TARGET_DIR)/product/core_64_bit.mk)
$(call inherit-product, $(SRC_TARGET_DIR)/product/full_base_telephony.mk)

# Inherit some common Lineage stuff.
$(call inherit-product, vendor/lineage/config/common_full_phone.mk)

# Inherit from aion device
$(call inherit-product, device/motorola/aion/device.mk)

PRODUCT_DEVICE := aion
PRODUCT_NAME := lineage_aion
PRODUCT_BRAND := motorola
PRODUCT_MODEL := motorola edge 2023
PRODUCT_MANUFACTURER := motorola

PRODUCT_GMS_CLIENTID_BASE := android-motorola

PRODUCT_BUILD_PROP_OVERRIDES += \
    PRIVATE_BUILD_DESC="aion_g_sys-user 14 U1TTS34M.146-62-3 ddede release-keys"

BUILD_FINGERPRINT := motorola/aion_g_sys/aion:14/U1TTS34M.146-62-3/ddede:user/release-keys
