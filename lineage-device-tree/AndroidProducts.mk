#
# Copyright (C) 2025 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_aion.mk

COMMON_LUNCH_CHOICES := \
    lineage_aion-user \
    lineage_aion-userdebug \
    lineage_aion-eng
